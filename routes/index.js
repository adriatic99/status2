var express = require('express');
var router = express.Router();
var soap = require('express-soap');
const TestService = require('../models/status');
const TestServiceInstance = new TestService();

/* GET home page. */
router.get('/:orderid/:creationdate', function(req, res, next) {
	
	var message;
	var status;
	var orderid = req.params['orderid'];
	var creationdate = req.params['creationdate'];
	if(orderid && creationdate) {
		console.log("start status:",status);
		TestServiceInstance.present(orderid, creationdate).then(function(result) {
			status = result;
			console.log(result);
			if (typeof status === 'string' || status instanceof String) {
				console.log("error returned");
				res.render('error', { msg: status });
			} else {
				res.render('index', { msg: status });
			}
		}, function(err) {
			console.log(err);
			res.render('error', { msg: err });
		});
	}
	else if(orderid) {
		message = "nedostaje creationdate";
		res.render('error', { msg: message });
	}
	else if(creationdate) {
		message = "nedostaje orderid";
		res.render('error', { msg: message });
	}
	else {
		message = "nedostaju parametri";
		res.render('error', { msg: message });
	}	
});

module.exports = router;
