var soap = require('soap');
const fs = require('fs') 
const readline = require('readline');
var dateFormat = require('dateformat');
//var Logger = require('./Logger');

//var logger = new Logger();

class TestService {

  constructor() {
	  this.config = {};
	  this.readFile();
  }

  readFile() {
	  
	try {
		// read contents of the file
		console.log("read data file ************************")
		const data = fs.readFileSync('models/status_configuration.txt', 'UTF-8');

		// split the contents by new line
		const lines = data.split(/\r?\n/);

		// print all lines
		lines.forEach((line) => {
			if(line.includes('ESBendpoint') || line.includes('ESBusername') || line.includes('ESBpassword') || line.includes('ESBwsdl')) {
				const [key, value] = line.split('==');
				this.config[key] = value;
			}
		});
	} catch (err) {
		console.error(err);
		config = null;
	}
  }

  /**
   * @description Attempt to create a post with the provided object
   * @param postToCreate {object} Object containing all required fields to
   * create post
   * @returns {Promise<{success: boolean, error: *}|{success: boolean, body: *}>}
   */
   present(sapordernumber, creationdate) {
	   
		if (!this.config || !this.config['ESBendpoint']) {
			return "oprostite, zbog tehničkih smetnji nismo u mogućnosti dati vam status vaše pošiljke";      
		}
    var currentdate = Date.now();
    var currentdatestr = dateFormat(currentdate, "yyyy-mm-dd");
    console.log("current date:", currentdatestr);
    console.log(this.config['ESBwsdl']);
		console.log(this.config['ESBendpoint']);
		console.log(this.config['ESBusername']);
		console.log(this.config['ESBpassword']);
		var username = this.config['ESBusername'];
		var password = this.config['ESBpassword'];
		var wsdlUrl = this.config['ESBwsdl'];
		
		var options = {endpoint: this.config['ESBendpoint'],
				strictSSL: false,
				disableCache: true,
				SOAPAction: "/Services/CRMWebServicesSAPSO.serviceagent/PortTypeEndpoint1/GetSAPDeliveryStatus"
				};
    return new Promise(function(resolve, reject) {
      
		soap.createClient(wsdlUrl, options, function(err, soapClient){
			console.log(JSON.stringify(soapClient.describe(), null, 2));
			soapClient.addHttpHeader('ESB-Username', username);
			soapClient.addHttpHeader('ESB-Password', password);
			
			if (err){
        reject(err);
			}
			
			var today = new Date().toISOString().slice(0, 10);
			console.log(today);
			
			var args = `
					  <crm:GetSAPDeliveryStatusRequest clientID="webshop" externalTransactionID="?">
						 <timeStart>00:00:00</timeStart>
						 <timeEnd>23:59:00</timeEnd>
						 <deliveryNumberSAP>` + sapordernumber + `</deliveryNumberSAP>
						 <dateStart>` + creationdate +`</dateStart>
						 <dateEnd>` + today + `</dateEnd>
					  </crm:GetSAPDeliveryStatusRequest>`;
			
			soapClient.GetSAPDeliveryStatus(
			{
				timeStart: '00:00:00',
				timeEnd : '23:59:00',
				deliveryNumberSAP : sapordernumber,
				dateStart: creationdate,
				dateEnd: currentdatestr,
				attributes: {
					clientID: 'webshop'
				}
			}
			/*{_xml:args}*/, function(err, result, raw, soapHeader){
					if (err){
						console.log(err);
						reject(err);
					}
					console.log('response:', result['ResponseBody']['item'][0]);
					resolve(result ['ResponseBody']['item']);
			});			
		});
    });
		//return "tehničke smetnje na sustavu, oprostite u ime A1";
  }
}

module.exports = TestService;